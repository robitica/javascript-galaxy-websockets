var url = "/";
var primus = Primus.connect(url, {
    reconnect: {
        max: Infinity // Number: The max delay before we try to reconnect.
      , min: 500 // Number: The minimum delay before we try reconnect.
      , retries: 10 // Number: How many times we should try to reconnect.
    }
  });

  primus.on('data', function(data) {
    let space = document.querySelector(".space");
    let indexAns1 = document.querySelector(".indexAns1");
    let indexAns2 = document.querySelector(".indexAns2");
    let indexQuestion = document.querySelector(".indexQuestion");
    let question = document.querySelector(".questionDone");
    let answer1 = document.querySelector(".answer1Done");
    let answer2 = document.querySelector(".answer2Done");
    if(question) {
      question.innerHTML = data.question;
      if(data.questionIndex == 0) {
        indexQuestion.innerHTML = data.questionIndex;
        space.innerHTML = "<img src='../images/terra.svg'/><img src='../images/mars.svg'/>";
      } else {
        indexQuestion.innerHTML = data.questionIndex;
        space.innerHTML = "<img src='../images/mars.svg'/><img src='../images/terra.svg'/>";
      }

      if(data.answer1Index == 0) {
        indexAns1.innerHTML = data.answer1Index;
        answer1.innerHTML = "<img src='../images/rocket-monkey.svg'/>";
      } else {
        indexAns1.innerHTML = data.answer1Index;
        answer1.innerHTML = "<img src='../images/rocket-pig.svg'/>";
      }

      if(data.answer2Index == 0) {
        indexAns2.innerHTML = data.answer2Index;
        answer2.innerHTML = "<img src='../images/rocket-pinguin.svg'/>";
      } else {
        indexAns2.innerHTML = data.answer2Index;
        answer2.innerHTML = "<img src='../images/rocket-duck.svg'/>";
      }
    }

    let scoreAns1 = document.querySelector(".scoreAns1");
    let scoreAns2 = document.querySelector(".scoreAns2");
    if(scoreAns1) {
      if(data.scoreAnswer1 == 90 || data.scoreAnswer2 == 90) {
        var overlay = document.querySelector(".go");
        overlay.style.display = 'block';
      }
      if(data.scoreAnswer1 || data.scoreAnswer2) {
        scoreAns1.innerHTML = data.scoreAnswer1;
        scoreAns2.innerHTML = data.scoreAnswer2;
        answer1.style.left = data.scoreAnswer1 + "%";
        answer2.style.left = data.scoreAnswer2 + "%";
      }
    }
  });

  if(document.querySelector(".submit")) {
    // klikken op de submit button -> naar server sturen
    document.querySelector(".submit").addEventListener("click", function(e) {
      primus.write({ 
        question: document.querySelector(".question").options[document.querySelector(".question").selectedIndex].text,
        questionIndex: document.querySelector(".question").selectedIndex, 
        answer1Index: document.querySelector(".answer1").selectedIndex,
        answer2Index: document.querySelector(".answer2").selectedIndex
      }); 
      e.preventDefault();
    });
  } else {
    document.querySelector(".answer1Done").addEventListener("click", function(e) {
      primus.write({ 
        question: document.querySelector(".questionDone").innerHTML, 
        questionIndex: document.querySelector(".indexQuestion").innerHTML, 
        answer1Index: document.querySelector(".indexAns1").innerHTML,
        answer2Index: document.querySelector(".indexAns2").innerHTML,
        answer1: document.querySelector(".answer1Done").innerHTML,
        answer2: document.querySelector(".answer2Done").innerHTML,
        scoreAnswer1: parseInt(document.querySelector(".scoreAns1").innerHTML) + 1,
        scoreAnswer2: parseInt(document.querySelector(".scoreAns2").innerHTML)
      });
      e.preventDefault();
    });

    // klikken op de submit button -> naar server sturen
    document.querySelector(".answer2Done").addEventListener("click", function(e) {
      primus.write({ 
        question: document.querySelector(".questionDone").innerHTML, 
        questionIndex: document.querySelector(".indexQuestion").innerHTML, 
        answer1Index: document.querySelector(".indexAns1").innerHTML,
        answer2Index: document.querySelector(".indexAns2").innerHTML,
        answer1: document.querySelector(".answer1Done").innerHTML,
        answer2: document.querySelector(".answer2Done").innerHTML,
        scoreAnswer1: parseInt(document.querySelector(".scoreAns1").innerHTML),
        scoreAnswer2: parseInt(document.querySelector(".scoreAns2").innerHTML) + 1
      });
      e.preventDefault();
    });
  }